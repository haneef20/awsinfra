# Terraform Repo that setups an  AWS VPC and an EKS Cluster

## Purpose

This repository is used to setup an AWS VPC and then create AWS EKS resource on that VPC.


## Description

Here we are creating an Infrastructure on AWS with kubernetes to host containerized applications on it. For this we will create a vpc and use EKS for our Kubernetes solution. The VPC will contain both private and public Subnets. EKS Worker nodes will be setup in private subnets and public subnets will used for services like AWS ALB etc.


![](images/Architecture.png)


## Usage Instructions
The Terraform files are present in vpc-eks folder . So to run them locally please switch to vpc-eks directory and execute the below commands. Here we are making use of gitlab httpd backed storage for storing terraform state file . 

```
cd vpc-eks
terraform plan
terraform apply
```
    

## Inputs

| Name | Description | Type | Default | Required |
| ---- | ----------- | ---- | ------- | -------- |
| aws_region| The region to deploy the resources to . | string | us-east-1 | no |
| vpc_name | The name of the vpc to be created  | string | - | yes |
| vpc_cidr_block | The VPC CIDR block | string | "10.0.0.0/16 | no |
| environment | Usually of 'development’, ‘test’, or ‘production’ | string | qa | no |
| product | The product the infra is associated with  | string | none | yes |
| vpc_availability_zone| A list of  VPC Availability Zones | string | ["us-east-1a","us-east-1b"] | no |
| vpc_public_subnets | List of the VPC Public Subnets | string | ["10.0.1.0/24","10.0.2.0/24"] | no |
| vpc_private_subnets | List of the VPC Private Subnets | string | ["10.0.3.0/24","10.0.4.0/24"] | no |
| vpc_enable_nat_gateway | Where nat gateway should be enabled for private outbound traffic  | bool | true | no |
| vpc_single_nat_gateway | Whether only single NAT   | bool | true | no |
| documentation | A link to documentation and/or history file | string | none | no |


## Outputs VPC 

| Name | Description 
| ---- | ----------- 
| vpc_id | The ID of the VPC |
| vpc_cidr_block | CIDR block of the VPC |
| private_subnets | List of Private subnet IDS |
| public_subnets  | List of public Subnets  | 
| nat_public_ips | NAT Gateway Elastic IPS  |



## Outputs EKS

| Name | Description | 
| ---- | ----------- |
| cluster_arn | Amazon Resource Name of the cluster |
|  cluster_certificate_authority_data | Base64 encoded certificate data | 
| cluster_endpoint | Endpoint for your Kubernetes API server |
| cluster_id  | The ID of the EKS cluster | 
| cluster_name | The name of the EKS cluster | 
| cluster_oidc_issuer_ur | The URL on the EKS cluster for the OpenID Connect identity provider | 
| oidc_provider_arn |  The ARN of the OIDC Provider |
| oidc_provider_short_arn| The ARN of the OIDC Provider splitting the oidc-provider string | 

### Improvements and Security Considerations.
* The API Server in the setup allows public access which may not be a good idea in terms of security. We should close it down to only private access or restrict it with required public IP addresses if possible.
* EC2 is used as the worker nodes here. Fargate may be an option to look into .
* Also should look into using openID connect in AWS to retrieve credentials instead of storing keys.





