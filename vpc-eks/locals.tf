# Define Local Values in Terraform
locals {
  name         = "${var.product}-${var.environment}"
  cluster_name = "${local.name}-eks-cluster"
  environment  = var.environment
  product      = var.product
  common_tags = {
    environment = local.environment
    product     = var.product
  }
}
